<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'id' => '1',
                'name' => 'testowy',
                'email' => 'test@test.com',
                'email_verified_at' => now(),
                'password' => bcrypt('test123!'),
                'remember_token' => Str::random(10),
                'money' => 99999999,
            ],
        ]);
    }
}
