<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Prize;
use Faker\Generator as Faker;

$factory->define(Prize::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'image' => 'addresOfImage',
        'price' => $faker->numberBetween(1, 10),
        'code' => $faker->numberBetween(1, 9999999),
        'status' => $faker->numberBetween(0, 2),
    ];
});
