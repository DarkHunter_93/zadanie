<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Chest;
use Faker\Generator as Faker;

$factory->define(Chest::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'image' => 'addressOfImage',
        'price' => $faker->numberBetween(1, 10),
    ];
});
