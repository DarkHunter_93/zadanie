<?php
return [
    'payment' => [
        '200' => 250,
        '100' => 150,
        '50' => 10,
    ],
    'front' => 'https://hairclinicadvisor.com/',
    'email_verified' => 'https://hairclinicadvisor.com/email/correct',
    'email_verification_resend' => 'https://hairclinicadvisor.com/email/resend',
    'reset_password' => 'https://hairclinicadvisor.com/password/reset/',

];
