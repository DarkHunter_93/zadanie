<?php

return [
    'status' => [
        0 => 'oczekujący',
        1 => 'wysłany',
        2 => 'odrzucony',
    ],
];
