<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rune extends Model
{
    protected $fillable = [
        'id',
        'name',
        'image',
        'price',
        'bonus',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'rune_user', 'rune_id', 'user_id');
    }
}
