<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'money',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function runes()
    {
        return $this->belongsToMany('App\Models\Rune', 'rune_user', 'rune_id', 'user_id');
    }

    public function prizes()
    {
        return $this->belongsToMany('App\Models\Prize', 'prize_user', 'prize_id', 'user_id');
    }

    public function chests()
    {
        return $this->belongsToMany('App\Models\Chest', 'chest_user', 'chest_id', 'user_id');
    }
}
