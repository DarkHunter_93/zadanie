<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chest extends Model
{
    protected $fillable = [
        'id',
        'name',
        'image',
        'price',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'prize_user', 'prize_id', 'user_id');
    }
}
