<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    protected $fillable = [
        'id',
        'name',
        'image',
        'price',
        'code',
        'status',
    ];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'prize_user', 'prize_id', 'user_id');
    }
}
