<?php

namespace App\Http\Services;

use App\Http\Requests\ChestBuyRequest;
use App\Http\Requests\PrizeBuyRequest;
use App\Http\Requests\RuneBuyRequest;
use App\Models\Chest;
use App\Models\Prize;
use App\Models\Rune;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class EquipmentService
{
    public function equipmentsList()
    {
        $equipment = User::with('chests')->with('prizes')->with('runes')->where('users.id', Auth::id())->first();

        return $equipment;
    }

    public function buyChest(ChestBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        $chest = Chest::find($data['id']);
        $this->subMoney($chest->price);

        $user->chests()->attach($data['id']);
    }

    public function buyPrize(PrizeBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        $prize = Prize::find($data['id']);
        $this->subMoney($prize->price);

        $user->prizes()->attach([$data['id']]);
    }

    public function buyRune(RuneBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        $rune = Rune::find($data['id']);
        $this->subMoney($rune->price);

        $user->runes()->attach($data['id']);
    }

    private function subMoney(int $price)
    {
        $user = Auth::user();
        if ($user->money < $price) {
            return abort(409, 'You don\'t have enough money');
        }

        $user->update(['money' => $user->money - $price]);
    }
}
