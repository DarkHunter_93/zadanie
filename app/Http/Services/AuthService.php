<?php

namespace App\Http\Services;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\UserStoreRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function register(UserStoreRequest $request)
    {
        $data = $request->validated();

        $user = User::create($data);
        $token = $user->createToken('Personal Access Token')->accessToken;

        return $token;
    }

    public function login(LoginRequest $request)
    {
        $request->validated();

        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials)) {
            return null;
        }

        $user = $request->user();
        $token = $user->createToken('Personal Access Token')->accessToken;

        return $token;
    }
}
