<?php

namespace App\Http\Resources;

use App\Models\Chest;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ChestCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Chest $chest) {
            return (new ChestResource($chest));
        });

        return parent::toArray($request);
    }
}
