<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChestBuyRequest;
use App\Http\Requests\PrizeBuyRequest;
use App\Http\Requests\RuneBuyRequest;
use App\Http\Resources\ChestCollection;
use App\Http\Resources\PrizeCollection;
use App\Http\Resources\RuneCollection;
use App\Http\Services\EquipmentService;

class EquipmentController extends Controller
{
    protected $equipmentService;

    public function __construct(EquipmentService $equipmentService)
    {
        $this->equipmentService = $equipmentService;
    }

    public function index()
    {
        $data = $this->equipmentService->equipmentsList();

        return response()->json([
            'chests' => new ChestCollection($data['chests']),
            'prizes' => new PrizeCollection($data['prizes']),
            'runes' => new RuneCollection($data['runes']),
        ]);
    }

    public function buyChest(ChestBuyRequest $request)
    {
        $this->equipmentService->buyChest($request);

        return response()->json(['message' => 'Chest was bought'], 200);
    }

    public function buyPrize(PrizeBuyRequest $request)
    {
        $this->equipmentService->buyPrize($request);

        return response()->json(['message' => 'Prize was bought'], 200);
    }

    public function buyRune(RuneBuyRequest $request)
    {
        $this->equipmentService->buyRune($request);

        return response()->json(['message' => 'Rune was bought'], 200);
    }
}
