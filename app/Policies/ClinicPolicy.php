<?php

namespace App\Policies;

use App\Models\Clinic;
use App\Models\Media;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClinicPolicy
{
    use HandlesAuthorization;

    public function updateLeads(User $user, Clinic $clinic)
    {
        return $user->hasRole('Clinic') && $user->id == $clinic->user_id;
    }

    public function email(User $user)
    {
        return $user->hasRole('Admin');
    }

    public function deletePhoto(User $user, Clinic $clinic, Media $media)
    {
        return ($user->hasRole('Admin') || $user == $clinic->user_id) && $clinic->media->contains($media->id);
    }

    public function showGallery(User $user, Clinic $clinic)
    {
        return ($user->hasRole('Clinic') && $user->id == $clinic->user_id)
            || $user->hasRole('Admin');
    }
}
