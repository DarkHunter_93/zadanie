<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group(['prefix' => 'auth',
    'middleware' => 'cors'],
    function () {
        Route::post('register', 'Auth\AuthController@register');
        Route::post('login', 'Auth\AuthController@login');
    });
Route::group([
    'middleware' => 'auth:api',
], function () {
    Route::get('logout', 'Auth\AuthController@logout');
    Route::get('equipment', 'EquipmentController@index');
    Route::post('chests', 'EquipmentController@buyChest');
    Route::post('prizes', 'EquipmentController@buyPrize');
    Route::post('runes', 'EquipmentController@buyRune');
});
